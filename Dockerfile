FROM python:3.8.10-slim-buster
COPY . .
RUN pip3 install -r requirements.txt
CMD python3 app.py
# ENV FLASK_APP=app.py
# CMD gunicorn --log-level debug --worker-class gevent --bind 0.0.0.0:5000 app:app